//getElements
//Button
const elShowInputLoanButton = document.getElementById('get-loan')
const elGetLoanButton = document.getElementById('loan-sum-button')
const elBankButton = document.getElementById('bank-button')
const elWorkButton = document.getElementById('work-button')
const elRepayLoanButton = document.getElementById('repay-loan-button')
const elBuyButton = document.getElementById('buy')

//User input
const elLoanSum = document.getElementById('loan-sum-input')
const elLaptopSelect = document.getElementById('laptops')

//Text
const elLoanResponse = document.getElementById('loan-response')
const elBankBalance = document.getElementById('bank-balance')
const elOutstandingLoan = document.getElementById('outstanding-loan')
const elPay = document.getElementById('pay')
const elFeatures = document.getElementById('features')
const elLaptopName = document.getElementById('laptop-name')
const elLaptopDesc = document.getElementById('laptop-desc')
const elLaptopPrice = document.getElementById('laptop-price')
const elBuyResponse = document.getElementById('buy-response')

//Section
const elLoanSumSection = document.getElementById('loan-sum')
const elLaptopSection = document.getElementById('laptop-info-section')

const elFeatureList = document.getElementById('feature-list')

const elLaptopImage = document.getElementById('laptop-image')


function Person(name, bank, loan, pay, loansBeforeComputer) {  //function for creating a person
    this.name = name
    this.bank = bank
    this.loan = loan
    this.pay = pay
    this.loansBeforeComputer = loansBeforeComputer
}

//Creating only one person in this project, but for future work all functions should take a person object as an argument
const donald = new Person('Donald Duck', 1000, 0, 0, false)

function Laptop(id, name, desc, price, image, features) {  //function for creating a laptop
    this.id = id
    this.name = name
    this.desc = desc
    this.price = price
    this.image = image
    this.features = features
}

const laptops = [
    new Laptop(1, 'Acer Chromebook 315 15,6" FHD touch', 'An easy and small PC to use on the go', 5190, 'pictures/1154532_1.webp',['Pentium N5030 Quad Core', '8 GB RAM', '128 GB SSD', 'Google Chrome OS']),
    new Laptop(2, 'ASUS ZenBook 13 UX325JA 13,3" FHD', 'A really good pc', 14490, 'pictures/1159825.webp',['Core i7-1065G7', '16 GB RAM', '512 GB SSD', 'NumberPad', 'Windows 10 Home']),
    new Laptop(3, 'HP EliteBook 840 G7 14" Full HD', 'A really good pc', 18790, 'pictures/1171391.webp',['SureView', 'Core i5-10210U', '16GB RAM', '256GB SSD', 'Windows 10 Pro']),
    new Laptop(4, 'MacBook Air 13 256GB', 'Some piece of crap, avoid at all cost', 12790, 'pictures/1174470.webp' ,['Apple 8-Core M1 CPU', '8GB RAM', '256GB SSD', 'Apple 7-Core GPU'])
]

for (const laptop of laptops) {  //adding all laptops to the select form
    const elLaptopOption = document.createElement('option')
    elLaptopOption.value = laptop.id
    elLaptopOption.innerText = laptop.name
    elLaptopSelect.appendChild(elLaptopOption)
}

(function () {  //initialize all values in html
    elBankBalance.innerText = `Balance: ${donald.bank} NOK`
    if (donald.loan !== 0) {
        elOutstandingLoan.innerText = `Outstanding loan: ${donald.loan} NOK`
        elRepayLoanButton.style.display = 'block'
    }
    elPay.innerText = `Pay: ${donald.pay} NOK`
})()

const updateTexts = () => {  //update all values in html
    elBankBalance.innerText = `Balance: ${donald.bank} NOK`
    if (donald.loan !== 0) {
        elOutstandingLoan.innerText = `Outstanding loan: ${donald.loan} NOK`
        elRepayLoanButton.style.display = 'block'
    } else {
        elOutstandingLoan.innerText = ''
        elRepayLoanButton.style.display = 'none'
    }
    elPay.innerText = `Pay: ${donald.pay} NOK`
    elLoanResponse.innerText = ''
}

elShowInputLoanButton.addEventListener('click', function() {  //check if person can take a new loan
    if (Number(donald.loan) !== 0){
        elLoanResponse.innerText = 'You must pay back your current loan before getting a new one'
    } else if(donald.loansBeforeComputer === true) {
        elLoanResponse.innerText = 'You can not get a new loan before buying a computer'
    } else {
        elLoanSumSection.style.display = 'block'
    }
})

elGetLoanButton.addEventListener('click', function() {  //gets a new loan
    if (elLoanSum.value > donald.bank * 2) {
        elLoanResponse.innerText = `You're request can not be higher than ${donald.bank * 2}`
    } else {
        donald.loan = elLoanSum.value
        elOutstandingLoan.innerText = `Outstanding loan: ${donald.loan} NOK`
        elLoanSumSection.style.display = 'none'
        elLoanResponse.innerText = "You're request has been fulfilled"
        elRepayLoanButton.style.display = 'block'
        donald.loansBeforeComputer = true
    }
})

elBankButton.addEventListener('click', function() {  //transfers pay money to bank balance
    if (donald.loan > 0) {
        if (donald.pay * 0.1 > donald.loan) {
            donald.bank += donald.pay - donald.loan
            donald.loan = 0
        } else {
            donald.loan -= donald.pay * 0.1
            donald.bank += donald.pay * 0.9
        }
    } else {
        donald.bank += donald.pay

    }
    donald.pay = 0
    updateTexts()
})

elWorkButton.addEventListener('click', function() {  //adds 100 to pay
    donald.pay += 100
    updateTexts()
})

elRepayLoanButton.addEventListener('click', function () {  //uses money from pay to pay loan
    if (donald.pay >= donald.loan) {
        donald.pay -= donald.loan
        donald.loan = 0
    } else {
        donald.loan -= donald.pay
        donald.pay = 0
    }
    updateTexts()
})

elLaptopSelect.addEventListener('change', function() {  //choose which laptop shown
    const value = Number(this.value)
    if (value === -1) {
        elLaptopSection.style.display = 'none'
        while (elFeatureList.hasChildNodes()) {
            elFeatureList.removeChild(elFeatureList.firstChild);
        }
        elFeatures.style.display = 'none'

    } else {
        elLaptopSection.style.display = 'block'
        elFeatures.style.display = 'block'
        const selectedLaptop = findSelectedLaptop(laptops, value)

        elLaptopSection.style.display = 'block'
        elLaptopImage.src = selectedLaptop.image
        elLaptopName.innerText = selectedLaptop.name
        elLaptopDesc.innerText = selectedLaptop.desc
        elLaptopPrice.innerText = `Price: ${selectedLaptop.price} NOK`

        while (elFeatureList.hasChildNodes()) {
            elFeatureList.removeChild(elFeatureList.firstChild);
        }

        for (const feature of selectedLaptop.features) {
            const elItem = document.createElement('li')
            elItem.innerText = feature
            elFeatureList.appendChild(elItem)
        }

        elBuyResponse.innerText = ''
    }

})

const findSelectedLaptop = (laptops, id) => {
    return laptops.find(function (laptop) {
        return laptop.id === Number(id)
    })
}

elBuyButton.addEventListener('click', function() {  //tries to buy a computer
    const value = Number(elLaptopSelect.value)
    const selectedLaptop = findSelectedLaptop(laptops, value)
    if (donald.bank >= selectedLaptop.price) {
        donald.bank -= selectedLaptop.price
        elBuyResponse.innerText = `You are now the owner of a ${selectedLaptop.name}`
        donald.loansBeforeComputer = false
    } else {
        elBuyResponse.innerText = `You do not have sufficient money to buy a ${selectedLaptop.name}`
    }
    updateTexts()
})